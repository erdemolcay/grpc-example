package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc/credentials"
	"log"
	"net"
	"os"

	"google.golang.org/grpc"
	pb "grpc-example/helloworld"
)

type server struct {
	pb.UnimplementedGreeterServer
	greeter           *grpc.Server
	grpcPort          int
	caCertFile        string
	serverCertFile    string
	serverCertKeyfile string
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Received: %v", in.GetName())
	return &pb.HelloReply{Message: "Hello " + in.GetName()}, nil
}

func main() {
	s := new(server)

	viper.AutomaticEnv()
	viper.SetEnvPrefix("grt")
	s.grpcPort = viper.GetInt("grpc_port")
	s.caCertFile = viper.GetString("ca_cert_file")
	s.serverCertFile = viper.GetString("server_cert_file")
	s.serverCertKeyfile = viper.GetString("server_cert_key_file")

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.grpcPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	caCert, err := os.ReadFile(s.caCertFile)
	if err != nil {
		log.Fatalf("failed to read ca cert file: %v", err)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(caCert) {
		log.Fatal("failed to append ca cert into cert pool")
	}

	cert, err := tls.LoadX509KeyPair(s.serverCertFile, s.serverCertKeyfile)
	if err != nil {
		log.Fatalf("failed to load certificate: %v", err)
	}
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certPool,
	}
	tlsCredentials := credentials.NewTLS(tlsConfig)

	s.greeter = grpc.NewServer(grpc.Creds(tlsCredentials))

	pb.RegisterGreeterServer(s.greeter, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.greeter.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
