FROM golang:1.21 as server
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -o /greeter-server greeter-server/*
CMD ["/greeter-server"]

FROM golang:1.21 as client
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -o /greeter-client greeter-client/*
CMD ["/greeter-client"]