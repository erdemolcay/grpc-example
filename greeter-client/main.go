package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc/credentials"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"google.golang.org/grpc"
	pb "grpc-example/helloworld"
)

type client struct {
	greeter           pb.GreeterClient
	serverAddr        string
	httpPort          int
	caCertFile        string
	clientCertFile    string
	clientCertKeyFile string
}

func (c client) greet(name string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.greeter.SayHello(ctx, &pb.HelloRequest{Name: name})
	if err != nil {
		return "", err
	}
	return r.GetMessage(), nil
}

func (c client) httpResponse(w http.ResponseWriter, req *http.Request) {
	name := req.URL.Query().Get("name")
	msg, err := c.greet(name)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		fmt.Fprintf(w, msg)
	}
}

func main() {
	c := new(client)

	viper.AutomaticEnv()
	viper.SetEnvPrefix("grt")
	c.serverAddr = viper.GetString("server_addr")
	c.httpPort = viper.GetInt("http_port")
	c.caCertFile = viper.GetString("ca_cert_file")
	c.clientCertFile = viper.GetString("client_cert_file")
	c.clientCertKeyFile = viper.GetString("client_cert_key_file")

	caCert, err := os.ReadFile(c.caCertFile)
	if err != nil {
		log.Fatalf("failed to read ca cert file: %v", err)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(caCert) {
		log.Fatal("failed to append ca cert into cert pool")
	}

	cert, err := tls.LoadX509KeyPair(c.clientCertFile, c.clientCertKeyFile)
	if err != nil {
		log.Fatalf("failed to load certificate: %v", err)
	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientCAs:    certPool,
		RootCAs:      certPool,
	}

	tlsCredential := credentials.NewTLS(tlsConfig)

	conn, err := grpc.Dial(c.serverAddr, grpc.WithTransportCredentials(tlsCredential))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	c.greeter = pb.NewGreeterClient(conn)

	http.HandleFunc("/", c.httpResponse)
	http.ListenAndServe(":"+strconv.Itoa(c.httpPort), nil)
}
